import { shallowMount } from '@vue/test-utils'
import TodoItem from '../../src/components/TodoItem.vue'

describe("TodoItem.vue",() => {
    it("renders correctly",() => {
        const wrapper = shallowMount(TodoItem, {
            propsData:{
                text:"this is a todo item",
                itemId: 1
            }
        });

        expect(wrapper.element).toMatchSnapshot();
    });

    it("emits todo-change event",() => {
        const wrapper = shallowMount(TodoItem, {
            propsData:{
                text:"this is a todo item",
                itemId: 1
            }
        });

        const f = jest.fn();
        wrapper.vm.$on("todo-change",f);
        wrapper.find("#todo-label-1").trigger("change");
        expect(f).toHaveBeenCalledTimes(1);
    });

    it("doesn't emit todo-change event on disabled checkboxes", () => {

        const wrapper = shallowMount(TodoItem, {
            propsData:{
                text:"this is a todo item",
                itemId: 1,
                disabled:true
            }
        });

        const f = jest.fn();
        wrapper.vm.$on("todo-change",f);
        wrapper.find("#todo-label-1").trigger("change");

        expect(f).toHaveBeenCalledTimes(0);
    });
});
