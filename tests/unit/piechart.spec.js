const puppeteer = require("puppeteer");
const process = require("process");

describe('PieChart.vue',() => {

    it('renders correctly, creates component screenshot at tests/screenshot/piechart.png', async () => {
      const browser = await puppeteer.launch();
      const page = await browser.newPage();
      await page.goto("file://" + process.cwd() + "/dist/index.html");
      await page.screenshot({path: 'tests/screenshot/piechart.png'});
      
      const titleText = await page.evaluate(() => document.getElementsByClassName("gtitle")[0].innerHTML);
      await browser.close();
      expect(titleText).toEqual("Electricity Use");
      
    });

});
