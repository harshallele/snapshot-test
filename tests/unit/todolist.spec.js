import { mount } from '@vue/test-utils'
import TodoList from '../../src/components/TodoList.vue'


describe('TodoList.vue',() => {
    it('renders correctly', () => {
        const wrapper  = mount(TodoList,{
            propsData:{
                items:[
                    {title: "title 1"},
                    {title: "title 2"},
                    {title: "title 3"}
                ]
            }
        });
        expect(wrapper.element).toMatchSnapshot();
    });

    it('emits the checkbox changed event when checkbox is checked',() => {
        const wrapper  = mount(TodoList,{
            propsData:{
                items:[
                    {title: "title 1"},
                    {title: "title 2"},
                    {title: "title 3"}
                ]
            }
        });
        const f = jest.fn();
        wrapper.vm.$on('checkbox-changed',f);
        wrapper.find("#todo-label-0").trigger("change");
        wrapper.find("#todo-label-1").trigger("change");
        expect(f).toHaveBeenCalledTimes(2);
    });
})
