import { mount } from '@vue/test-utils'
import Form from '../../src/components/Form.vue'


describe('Form.vue', () => {


    it('renders correctly',() => {
        const wrapper = mount(Form);
        expect(wrapper.element).toMatchSnapshot();
    });

    it('Fill Data button works correctly',() => {
        const wrapper = mount(Form);
        wrapper.find("#btnFillData").trigger("click");
        
        expect(wrapper.vm.name).toEqual("John Smith");
        expect(wrapper.vm.email).toEqual("johnsmith@gmail.com");
        expect(wrapper.vm.select).toEqual("2");
    })
})
